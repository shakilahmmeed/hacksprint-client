FROM node:14.10.1 as build-step

WORKDIR /app
COPY package.json /app
RUN npm install
COPY . /app
RUN npm run build
EXPOSE 5000
CMD npm run serve


# FROM nginx:1.17.1-alpine
# COPY --from=build-step /app/build /var/www/frontend
# EXPOSE 80
# COPY --from=build-step /app/build /usr/share/nginx/html