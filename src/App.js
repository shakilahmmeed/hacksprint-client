import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./store";
import Home from "./pages/Home";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Dashboard from "./pages/Dashboard";
import ChallengePage from "./pages/Challenges";
import ChallengeDetail from "./pages/ChallengeDetail";
import Jobs from "./pages/Jobs";
import JobDetail from "./pages/JobDetail";
import Notification from "./pages/Notification";
import TrackPage from "./pages/Tracks";
import PracticePage from "./pages/Practices";
import Profile from "./pages/Profile";
import Settings from "./pages/Settings";
import SinglePractice from "./pages/PracticeDetail";
import Playground from "./pages/Playground";
import VerificationSuccess from "./pages/VerificationSuccess";
import VerificationFailed from "./pages/VerificationFailed";
import NotFound from "./pages/404";
import PrivateRoute from "./pages/PrivateRoute";
import { loadUser } from "./actions/authAction";
import "./App.scss";

export default class App extends Component {
	constructor(props) {
		super(props);
		store.dispatch(loadUser());
	}

	render() {
		return (
			<Provider store={store}>
				<Router>
					<Switch>
						<Route exact path="/" component={Home} />
						<Route exact path="/register" component={Register} />
						<Route exact path="/login" component={Login} />
						<Route exact path="/logout" component={Logout} />

						<PrivateRoute exact path="/dashboard" component={Dashboard} />
						<PrivateRoute exact path="/challenges" component={ChallengePage} />
						<PrivateRoute
							exact
							path="/challenges/:challengeSlug"
							component={ChallengeDetail}
						/>
						<PrivateRoute exact path="/jobs" component={Jobs} />
						<PrivateRoute exact path="/jobs/:jobSlug" component={JobDetail} />
						<PrivateRoute exact path="/notification" component={Notification} />
						<PrivateRoute exact path="/tracks" component={TrackPage} />
						<PrivateRoute
							exact
							path="/tracks/:trackSlug"
							component={PracticePage}
						/>
						<PrivateRoute
							exact
							path="/tracks/:trackSlug/:practiceSlug"
							component={SinglePractice}
						/>
						<PrivateRoute exact path="/profile" component={Profile} />
						<PrivateRoute exact path="/settings" component={Settings} />

						<Route exact path="/playground" component={Playground} />
						<Route exact path="/success" component={VerificationSuccess} />
						<Route exact path="/failed" component={VerificationFailed} />

						<Route path="*" component={NotFound} />
					</Switch>
				</Router>
			</Provider>
		);
	}
}
