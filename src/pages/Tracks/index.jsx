import React, { Component } from "react";
import { connect } from "react-redux";
import axios from "axios";
import { Link, useRouteMatch } from "react-router-dom";
import Navbar from "../../components/Navbar/DashNav";
import AdOne from "../../components/Ad/AdOne";
import Sidebar from "../../components/Sidebar/Sidebar";
import Footer from "../../components/Footer/Footer";

class TrackPage extends Component {
	state = {
		tracks: [],
	};

	componentDidMount() {
		axios
			.get(`${process.env.REACT_APP_WEBSITE_NAME}/api/tracks/`, {
				headers: {
					Authorization: `Token ${this.props.token}`,
				},
			})
			.then((res) => {
				const tracks = res.data;
				this.setState({ tracks });
			});
	}

	render() {
		return (
			<div>
				<Navbar />
				<div className="pt-4 pb-5 bg-color">
					<div className="container">
						<div className="ui two column grid">
							<div className="twelve wide column">
								<div className="tracks">
									<div className="section-title">
										<h2>All Tracks</h2>
										<div className="ui three cards mt-1">
											{this.state.tracks.map((track) => (
												<TrackCard key={track.id} track={track} />
											))}
										</div>
									</div>
								</div>
							</div>
							<div className="four wide column mt-5">
								<AdOne />
								<Sidebar />
							</div>
						</div>
					</div>
				</div>
				<Footer />
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		token: state.authReducer.token,
	};
};

export default connect(mapStateToProps)(TrackPage);

function TrackCard(props) {
	let { url } = useRouteMatch();

	return (
		<div className="card">
			<Link
				to={url + "/" + props.track.slug}
				className="content"
				style={{ color: "inherit" }}
			>
				<div className="ui two column grid">
					<div className="six wide column pr-0">
						<img
							src={props.track.avatar}
							className="img-fluid"
							style={{ width: "100%", height: "auto" }}
							alt="dummy"
						/>
					</div>
					<div className="ten wide column">
						<h4 className="mb-2">{props.track.title}</h4>
						<div className="description">{props.track.desc}</div>
					</div>
				</div>
			</Link>
		</div>
	);
}
