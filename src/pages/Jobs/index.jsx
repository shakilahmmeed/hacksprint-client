import React, { Component } from "react";
import axios from "axios";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import Navbar from "../../components/Navbar/DashNav";
import "./Jobs.scss";

class Jobs extends Component {
	state = {
		jobs: [],
	};

	componentDidMount() {
		axios
			.get(`${process.env.REACT_APP_WEBSITE_NAME}/api/jobs/`, {
				headers: {
					Authorization: `Token ${this.props.token}`,
				},
			})
			.then((res) => {
				console.log(res.data);
				this.setState({ jobs: res.data });
			})
			.catch((err) => console.log(err));
	}

	render() {
		return (
			<div>
				<Navbar />
				<section
					className="search-area"
					style={{
						backgroundImage:
							"url(https://hrcdn.net/fcore/assets/svgs/world_map-e1ca0120b6.svg)",
					}}
				>
					<div className="search-shadow">
						<div className="d-flex justify-content-center align-items-center h-100 w-100">
							<h1 style={{ color: "#f1f1f1" }}>Find Your Dream Job</h1>
						</div>
					</div>
				</section>
				<section className="py-5 jobs-area">
					<div className="container">
						<div className="row">
							{this.state.jobs.map((job) => (
								<div className="col-md-4 my-3" key={job.id}>
									<Link to={this.props.match.url + "/" + job.slug}>
										<JobCard job={job} />
									</Link>
								</div>
							))}
						</div>
					</div>
				</section>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		token: state.authReducer.token,
	};
};

export default connect(mapStateToProps)(Jobs);

class JobCard extends Component {
	render() {
		return (
			<div className="row no-gutters job">
				<div className="col-md-4">
					<div
						className="logo"
						style={{
							backgroundImage:
								"url(https://futurestartup.com/wp-content/uploads/2019/10/augmedix.jpg)",
						}}
					></div>
				</div>
				<div className="col-md-8">
					<div className="job-content">
						<h3 className="title">{this.props.job.title}</h3>
						<div className="d-flex mt-1">
							<i className="building outline icon"></i>
							<p className="pl-1">Augmedix Limited</p>
						</div>
						<div className="d-flex mt-1">
							<i className="map marker alternate icon"></i>
							<p className="pl-1">{this.props.job.location}</p>
						</div>
						<div className="d-flex mt-1">
							<i className="briefcase icon"></i>
							<p className="pl-1">{this.props.job.employment_status}</p>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
