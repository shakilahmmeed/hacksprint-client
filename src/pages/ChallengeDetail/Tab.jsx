import React, { Component, Fragment } from "react";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import axios from "axios";

export default class Tabe extends Component {
	render() {
		const { is_joined } = this.props.content;

		return (
			<Fragment>
				<Tabs>
					<TabList className="mb-3 tab-bar">
						<div className="d-flex">
							<Tab className="tab">Details</Tab>
							<Tab className="tab">Leaderboard</Tab>
							{is_joined ? <Tab className="tab">Team</Tab> : ""}
							{is_joined ? (
								<div className="ml-auto close-button">End Challenge</div>
							) : (
								<div
									className="ml-auto join-button"
									onClick={this.props.handleJoinClick}
								>
									Join Challenge
								</div>
							)}
						</div>
					</TabList>
					<TabPanel>
						<DetailTab body={this.props.content.body} />
					</TabPanel>
					<TabPanel>
						<Leaderboard slug={this.props.slug} token={this.props.token} />
					</TabPanel>
					<TabPanel>
						<Team slug={this.props.slug} token={this.props.token} />
					</TabPanel>
				</Tabs>
			</Fragment>
		);
	}
}

function DetailTab(props) {
	let checkpoints = [
		{ title: "Must've 3 Routes", status: "partial" },
		{ title: "Contact Page Form", status: "approved" },
		{ title: "Calculation", status: "error" },
		{ title: "Result On Next Page", status: "initial" },
		{ title: "Must've Javascript Version", status: "approved" },
		{ title: "Upload To Heroku", status: "error" },
	];

	return (
		<div className="ui grid">
			<div className="eleven wide column">
				<div className="ui card w-100">
					<div className="content">
						<div
							dangerouslySetInnerHTML={{
								__html: props.body,
							}}
						></div>
					</div>
				</div>
			</div>
			<div className="five wide column pl-0">
				<div className="ui card w-100">
					<div className="content">
						<h4>Checkpoints</h4>
						<div className="ui middle aligned selection list">
							{checkpoints.map((checkpoint) => (
								<Checkpoint data={checkpoint} />
							))}
						</div>
					</div>
				</div>
				<div className="ui action input w-100">
					<input type="text" placeholder="Submission Link" />
					<button className="ui teal button">Add</button>
				</div>
			</div>
		</div>
	);
}

function Checkpoint(props) {
	const { title, status } = props.data;
	const statusIcon =
		status == "partial"
			? "https://image.flaticon.com/icons/svg/148/148768.svg"
			: status == "approved"
			? "https://image.flaticon.com/icons/svg/811/811868.svg"
			: status == "error"
			? "https://image.flaticon.com/icons/svg/753/753345.svg"
			: "https://image.flaticon.com/icons/svg/189/189665.svg";

	return (
		<div className="item">
			<img className="ui avatar image check" src={statusIcon} />
			<div className="content">
				<div className="header">{title}</div>
			</div>
			<div className="right floated content">
				{status == "initial" ? (
					<Check icon="check" />
				) : status == "approved" ? (
					""
				) : (
					<Check icon="redo" />
				)}
			</div>
		</div>
	);
}

function Check(props) {
	let { icon } = props;
	return (
		<button className="mini ui icon button">
			<i class={`${icon} icon`}></i>
		</button>
	);
}

class Leaderboard extends Component {
	state = {
		teams: [],
	};

	componentDidMount() {
		axios
			.get(
				`${process.env.REACT_APP_WEBSITE_NAME}/api/challenges/${this.props.slug}/teams/`,
				{
					headers: {
						Authorization: `Token ${this.props.token}`,
					},
				}
			)
			.then((res) => {
				this.setState({ teams: res.data });
			});
	}

	render() {
		return (
			<table className="ui single line table">
				<thead>
					<tr>
						<th>Rank</th>
						<th>Team Name</th>
						<th>Task</th>
						<th>University</th>
					</tr>
				</thead>
				<tbody>
					{this.state.teams.map((team, index) => (
						<tr>
							<td>{index + 1}</td>
							<td>{team.name}</td>
							<td>***</td>
							<td>DIU</td>
						</tr>
					))}
				</tbody>
			</table>
		);
	}
}

class Team extends Component {
	state = {
		team: {},
	};

	componentDidMount() {
		axios
			.get(
				`${process.env.REACT_APP_WEBSITE_NAME}/api/challenges/${this.props.slug}/myteam/`,
				{
					headers: {
						Authorization: `Token ${this.props.token}`,
					},
				}
			)
			.then((res) => {
				this.setState({ team: res.data });
			});
	}

	render() {
		return (
			<div>
				<div className="ui card w-100">
					<div className="content">
						<div className="header">Team Name</div>
					</div>
					<div className="content">
						<div className="ui right action input">
							<input type="text" value={this.state.team.name} />
							<button className="ui teal button">Save Team Name</button>
						</div>
					</div>
				</div>
				<div className="ui card w-100">
					<div className="content d-flex justify-content-between align-items-center">
						<div className="header d-inline-block">Team Members</div>
						<div className="d-inline-block ml-auto">
							<div className="ui action left icon input">
								<i className="search icon"></i>
								<input type="text" placeholder="Add Team Member" />
								<div className="ui teal button">Add</div>
							</div>
						</div>
					</div>
					<div className="content">
						<table className="ui very basic table">
							<thead>
								<tr>
									<th>Name</th>
									<th>Title</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody>
								{this.state.team.member &&
									this.state.team.member.map((member) => (
										<tr>
											<td>{`${member.first_name} ${member.last_name}`}</td>
											<td>Leader</td>
											<td>Joined</td>
										</tr>
									))}
							</tbody>
						</table>
					</div>
				</div>
			</div>
		);
	}
}
