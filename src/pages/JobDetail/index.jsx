import React, { Component } from "react";
import axios from "axios";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import Navbar from "../../components/Navbar/DashNav";
import "./job_detail.scss";

class JobDetail extends Component {
	state = {
		jobs: [],
	};

	componentDidMount() {
		axios
			.get(`${process.env.REACT_APP_WEBSITE_NAME}/api/jobs/`, {
				headers: {
					Authorization: `Token ${this.props.token}`,
				},
			})
			.then((res) => {
				console.log(res.data);
				this.setState({ jobs: res.data });
			})
			.catch((err) => console.log(err));
	}

	render() {
		return (
			<div>
				<Navbar />
				<div className="container">
					<h1>Hello World!</h1>
				</div>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		token: state.authReducer.token,
	};
};

export default connect(mapStateToProps)(JobDetail);
